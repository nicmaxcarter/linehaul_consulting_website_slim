<?php

return function (Slim\App $app) {

    $app->get('/', ['\App\Controllers\RouteController', 'getHome'])->setName('home');
    $app->get('/availableFedExRoutes', ['\App\Controllers\RouteController', 'renderRoutes'])->setName('routes.available');
    $app->get('/soldFedExRoutes', ['\App\Controllers\RouteController', 'renderSold'])->setName('routes.sold');
    $app->get('/sellFedExRoutes', ['\App\Controllers\RouteController', 'renderSell'])->setName('routes.sell');
    $app->get('/route/{id}', ['\App\Controllers\RouteController', 'renderRouteIndividual'])->setName('route.individual');
    $app->get('/resources', ['\App\Controllers\RouteController', 'renderResources'])->setName('route.individual');
    $app->get('/contact', ['\App\Controllers\RouteController', 'renderContact'])->setName('contact');
    $app->get('/admin', ['\App\Controllers\RouteController', 'renderAdmin'])->setName('admin');
    $app->get('/admin/', ['\App\Controllers\RouteController', 'renderAdmin']);
    //$app->get('/cms', ['\App\Controllers\RouteController', 'renderCMS'])->setName('cms');
    //$app->get('/cms/', ['\App\Controllers\RouteController', 'renderCMS']);
    $app->get('/maintenance', ['\App\Controllers\RouteController', 'renderMaintenance'])->setName('maintenance');
    $app->get('/maintenance/', ['\App\Controllers\RouteController', 'renderMaintenance']);

    $app->post('/addsubscriber', ['\App\Controllers\APIController', 'addSubscriber']);
    $app->post('/submitContact', ['\App\Controllers\APIController', 'submitContactForm']);
    $app->post('/routes/{status}', ['\App\Controllers\APIController', 'getRoutesDefault']);
    $app->post('/routes/{status}/{order}/{category}', ['\App\Controllers\APIController', 'getRoutesSorted']);
    $app->post('/similarRoutes/{price}/{category}', ['\App\Controllers\APIController', 'getSimilarRoutesByPrice']);
    $app->post('/routeInfo/{id}', ['\App\Controllers\APIController', 'getRoute']);

};

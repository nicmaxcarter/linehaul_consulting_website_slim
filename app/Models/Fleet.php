<?php

namespace App\Models;

use Delight\Auth\Auth as Auth;
use Doctrine\DBAL\Connection as DB;

class Fleet
{

    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getTruckId($fxg_num)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id')
            ->from('truck')
            ->where('fxg_num = (:num)')
            ->setParameter('num', $fxg_num);

        return $query->execute()->fetch()['id'];
    }

    public function getTruckDisplayProfile($truckNum){
        $query = $this->db->createQueryBuilder();

        $query
            ->select('t.*, e.name, e.id as entity_key,e.entity_id')
            ->from('truck', 't')
            ->where('fxg_num = (:num)')
            ->join('t', 'entity', 'e','t.entity = e.id')
            ->setParameter('num', $truckNum);

        return $query->execute()->fetch();
    }

    public function createTruck($newTruck)
    {
        // add make to existing list
        try {
            $this->addMake($newTruck['make']);
        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {}

        // add model to existing list
        try {
            $this->addModel($newTruck['model']);
        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {}

        $primaryKey = $this->storeTruck($newTruck);

        return "New Truck has been added: id: $primaryKey, fxg_num: " . $newTruck['fxg_num'];
    }

    public function storeTruck($newTruck)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->insert('truck')
            ->values(
                array(
                    'vin' => '?',
                    'year' => '?',
                    'make' => '?',
                    'model' => '?',
                    'entity' => '?',
                    'fxg_num' => '?',
                    'odometer' => '?'
                )
            )
            ->setParameter(0, $newTruck['vin'])
            ->setParameter(1, $newTruck['year'])
            ->setParameter(2, $newTruck['make'])
            ->setParameter(3, $newTruck['model'])
            ->setParameter(4, $newTruck['entity'])
            ->setParameter(5, $newTruck['fxg_num'])
            ->setParameter(6, $newTruck['odometer']);

        $query->execute();

        return  $this->db->lastInsertId();
    }

    public function addMake($make)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->insert('truck_make')
            ->values(
                array(
                    'name' => '?',
                )
            )
            ->setParameter(0, $make);

        $query->execute();
    }
    public function addModel($model)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->insert('truck_model')
            ->values(
                array(
                    'name' => '?',
                )
            )
            ->setParameter(0, $model);

        $query->execute();
    }

    public function createUser($newUser)
    {

        try {
            $userId = $this->registerUser($newUser);
            $this->storeUserInformation($userId, $newUser['fname'], $newUser['lname']);
            $this->addUserAssociation($userId, $newUser['level'], $newUser['entity_id']);
            $this->addUserPermissions($userId, $newUser['level']);

            return $userId;
        } catch (\Delight\Auth\InvalidEmailException $e) {
            return ('Invalid email address');
        } catch (\Delight\Auth\InvalidPasswordException $e) {
            return ('Invalid password');
        } catch (\Delight\Auth\UserAlreadyExistsException $e) {
            return ('User already exists');
        } catch (\Delight\Auth\TooManyRequestsException $e) {
            return ('Too many requests');
        }
    }

    // returns trucks related to the entities it is provided
    // entities must be structured as an array of primary key ids
    public function getRelatedTrucksFiltered($entityKeys)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('t.id,t.fxg_num,t.year,t.make,t.odometer,e.name, t.perc_completed as perc')
            ->from('truck', 't')
            ->orderBy('fxg_num', 'ASC')
            ->where('entity IN (:ids)')
            ->join('t', 'entity', 'e','t.entity = e.id')
            ->setParameter('ids', $entityKeys, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        $result = $query->execute()->fetchAll();

        foreach ($result as $key=>$value) {
            $perc = $value['perc'] *100;
            $result[$key]['perc'] = $perc;
            if($perc < 70){
                $result[$key]['color'] = 'danger';
            }
            elseif($perc < 90){
                $result[$key]['color'] = 'warning';
            }
            else{
                $result[$key]['color'] = 'success';
            }
        }

        return $result;
    }

    public function getAllMakes()
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, name')
            ->from('truck_make');

        return $query->execute()->fetchAll();
    }

    public function getPercThroughPM($truck_key, $curr_odometer){

        // get the most recent pm
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, mileage, date')
            ->from('maint_event')
            ->orderBy('date','DESC')
            ->where('maint_service = (:service) AND truck = (:truck)')
            ->setParameter('service','34')
            ->setParameter('truck',$truck_key);

        $lastPM =  floatval($query->execute()->fetch()['mileage']);

        // get the pm lifespan
        $query = $this->db->createQueryBuilder();

        $query
            ->select('miles')
            ->from('maint_interval')
            ->where('maint_service = (:service)')
            ->setParameter('service','34');

        $frequency =  floatval($query->execute()->fetch()['miles']);


        // $nextPM = $lastPM + $frequency;
        $nextPM = $lastPM + $frequency;

        $milesToPM = $nextPM - $curr_odometer;

        if($nextPM < $curr_odometer){
            return 100;
        } else{
            return number_format($milesToPM/$frequency *100,0);
        }

    }

    public function getCurrentMileage($truck_key) {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('odometer')
            ->from('truck')
            ->where('id = (:key)')
            ->setParameter('key', $truck_key);

        return floatval($query->execute()->fetch()['odometer']);
    }

}

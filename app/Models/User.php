<?php

namespace App\Models;

use Delight\Auth\Auth as Auth;
use Doctrine\DBAL\Connection as DB;

class User
{

    protected $auth;
    protected $db;

    public function __construct(Auth $auth, DB $db)
    {
        $this->auth = $auth;
        $this->db = $db;
    }


    public function temp(){
        $userId = $this->auth->getUserId();
        try {
            $this->auth->admin()->addRoleForUserById($userId, \App\Models\MyRole::VENDOR);
            echo "success";
        }
        catch (\Delight\Auth\UnknownIdException $e) {
            // unknown user ID
            echo "whoops";
        }
    }
    public function createUser($newUser)
    {

        try {
            $userId = $this->registerUser($newUser);
            $this->storeUserInformation($userId, $newUser['fname'], $newUser['lname']);
            $this->addUserAssociation($userId, $newUser['level'], $newUser['entity_id']);
            $this->addUserPermissions($userId, $newUser['level']);

            return $userId;
        } catch (\Delight\Auth\InvalidEmailException $e) {
            return ('Invalid email address');
        } catch (\Delight\Auth\InvalidPasswordException $e) {
            return ('Invalid password');
        } catch (\Delight\Auth\UserAlreadyExistsException $e) {
            return ('User already exists');
        } catch (\Delight\Auth\TooManyRequestsException $e) {
            return ('Too many requests');
        }
    }

    public function registerUser($newUser)
    {
        $userId = $this->auth->register(
            $newUser['email'],
            $newUser['password'],
            $newUser['username']
        );

        return $userId;
    }

    public function storeUserInformation($id, $fname, $lname)
    {

        $query = $this->db->createQueryBuilder();

        $query
            ->insert('user_info')
            ->values(
                array(
                    'user_id' => '?',
                    'first_name' => '?',
                    'last_name' => '?',
                )
            )
            ->setParameter(0, $id)
            ->setParameter(1, $fname)
            ->setParameter(2, $lname);

        $query->execute();
    }
    public function addUserAssociation($id, $level, $entity)
    {

        // echo "User: $id<br>Level: $level<br>Entity: $entity";
        // die();
        $query = $this->db->createQueryBuilder();

        $query
            ->insert('user_association')
            ->values(
                array(
                    'user_id' => '?',
                    'user_role' => '?',
                    'entity' => '?',
                )
            )
            ->setParameter(0, $id)
            ->setParameter(1, $level)
            ->setParameter(2, $entity);

        $query->execute();
    }

    public function addUserPermissions($userId, $level)
    {
        switch ($level) {
            case '1':
                $this->auth->admin()->addRoleForUserById($userId, \App\Models\MyRole::OWNER);
            case '2':
                $this->auth->admin()->addRoleForUserById($userId, \App\Models\MyRole::MANAGER);
            case '3':
                $this->auth->admin()->addRoleForUserById($userId, \App\Models\MyRole::MANAGER);
            case '4':
                $this->auth->admin()->addRoleForUserById($userId, \App\Models\MyRole::MANAGER);
            case '5':
                $this->auth->admin()->addRoleForUserById($userId, \App\Models\MyRole::VENDOR);
        }
    }

    public function signin($email, $password)
    {
        try {
            $this->auth->login($email, $password);
            // $this->setDisplayTitles();
            $_SESSION['displayName'] = $this->getDisplayName();
            return true;
        } catch (\Delight\Auth\InvalidEmailException $e) {
            return 'Wrong email address';
        } catch (\Delight\Auth\InvalidPasswordException $e) {
            return 'Wrong password';
        } catch (\Delight\Auth\EmailNotVerifiedException $e) {
            return 'Email not verified';
        } catch (\Delight\Auth\TooManyRequestsException $e) {
            return 'Too many requests';
        }
    }

    public function signout()
    {
        try {
            $this->auth->logOutEverywhere();
            return true;
        } catch (\Delight\Auth\NotLoggedInException $e) {
            return "Something went wrong";
        }
    }

    public function setPassword($password)
    {
        $this->update([
            'password' => password_hash($password, PASSWORD_DEFAULT),
        ]);
    }

    // get all users that are under this user
    public function getRelatedUsers()
    {
        // get the user id of the logged in user
        $id = $this->auth->getUserId();

        $query = $this->db->createQueryBuilder();

        $query
            ->select('*')
            ->from('users')
            ->where('id != ?')
            ->setParameter(0, $id);

        return $query->execute()->fetchAll();

    }

    public function getRelatedEntitiesOriginal()
    {
        // $id = $this->auth->getUserId();

        $query = $this->db->createQueryBuilder();

        $query
            ->select('*')
            ->from('entity');

        return $query->execute()->fetchAll();
    }

    public function getRelatedEntities()
    {
        // get list of all associations where this user's key is related
        // var_dump($this->getAssociatedEntityKeys($this->auth->getUserId()));
        // die();
        return $this->getAssociatedEntityKeys($this->auth->getUserId());
    }

    public function getRelatedEntitiesFull()
    {
        // get the user primary key
        $id = $this->auth->getUserId();

        // get list of all associations where this user's key is related
        return $this->getAssociatedEntityKeysAndNames($id);
    }

    public function getRelatedEntitiesForVendor(){

        $keys = $this->getRelatedEntities();

        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, entity_id, name')
            ->from('entity')
            ->where('id IN (:ids)')
            ->setParameter('ids', $keys, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        return $query->execute()->fetchAll();
    }

    // given an array of entity keys
    // returns id, entity_id and entity name
    public function getFilteredEntities($keys) {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, entity_id, name')
            ->from('entity')
            ->where('id IN (:ids)')
            ->setParameter('ids', $keys, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        return $query->execute()->fetchAll();
    }

    public function getAssociatedEntityKeysAndNames($userKey){

        $keys = $this->getAssociatedEntityKeys($userKey);

        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, name')
            ->from('entity')
            ->where('id IN (:ids)')
            ->setParameter('ids', $keys, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        $result = $query->execute()->fetchAll();

       return $result;
    }

    public function getAssociatedEntityKeys($userKey){
        $query = $this->db->createQueryBuilder();

        $query
            ->select('entity')
            ->from('user_association')
            ->where('user_id = ?')
            ->setParameter(0, $userKey);

        $result = $query->execute()->fetchAll();
        $listOfEntityKeys = [];

        foreach($result as $entity){
            $listOfEntityKeys[] = $entity['entity'];   
        }

        return $listOfEntityKeys;
    }

    public function getUserAssociations() {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('a.id,a.user_id,r.title,e.name as entity_name')
            ->from('user_association','a')
            ->where('user_id = (:userkey)')
            ->join('a', 'entity', 'e', 'a.entity = e.id')
            ->join('a', 'user_role', 'r', 'a.user_role = r.id')
            ->setParameter('userkey', $this->auth->getUserId());

        $result = $query->execute()->fetchAll();

        if(sizeof($result) < 1){
            return false;
        } else {
            return $result;
        }
    }

    public function getAvailableRoles()
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('*')
            ->from('user_role');

        return $query->execute()->fetchAll();
    }

    public function getUserId(){
        return $this->auth->getUserId();
    }

    public function getDisplayName()
    {
        $query = $this->db->createQueryBuilder();
        $userId = $this->auth->getUserId();
        $query
            ->select('user_info.first_name', 'user_info.last_name')
            ->from('user_info')
            ->where('user_info.user_id = ?')
            ->setParameter(0, $userId);

        $result = $query->execute()->fetch();

        return $result['first_name'] . " " . $result['last_name'];
    }


    public function getUserRole(){
        if($this->auth->hasRole(\App\Models\MyRole::OWNER)){
            return 1;
        }
        if($this->auth->hasRole(\App\Models\MyRole::MANAGER)){
            return 'manager';
        }
        if($this->auth->hasRole(\App\Models\MyRole::VENDOR)){
            return 'vendor';
        }
    }
    public function getUserPermission(){
        if($this->auth->hasRole(\App\Models\MyRole::OWNER)){
            return 'owner';
        }
        if($this->auth->hasRole(\App\Models\MyRole::MANAGER)){
            return 'manager';
        }
        if($this->auth->hasRole(\App\Models\MyRole::VENDOR)){
            return 'vendor';
        }
    }
    // public function setDisplayTitles(){
    //     $query = $this->db->createQueryBuilder();
    //     $userId = $this->auth->getUserId();
    //     $query
    //         ->select('user_info.first_name', 'user_info.last_name', 'user_role.title')
    //         ->from('user_info')
    //         ->where('user_info.user_id = ?')
    //         ->join('user_role', 'user_role.id = ?')
    //         ->setParameter(0, $userId)
    //         ->setParameter(1, $userId);

    //     return $result = $query->execute()->fetch();

       

    //     return $result['first_name'] . " " . $result['last_name'];
    //         $_SESSION['displayName'] = $this->getDisplayName();
    //         $_SESSION['displayAssociation'] = $this->getAssociationTitle();
    // }

}

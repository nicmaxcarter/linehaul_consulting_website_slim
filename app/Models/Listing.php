<?php

namespace App\Models;

use Doctrine\DBAL\Connection as DB;

class Listing
{

    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getIndividualListing($id)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('*')
            ->from('listing')
            ->where('id = (:id)')
            ->setParameter('id', $id);

        return $query->execute()->fetch();
    }

    public function getListings($status, $order, $category)
    {
        $pending = 'test';
        if ($status == 'available') {
            $pending = 'pending';
        }
        if ($order == 'none') {
            $sort = 'id';
            $order = 'DESC';
        } else {
            $sort = 'listing_price';
        }
        if ($category == 'none') {
            $category = '';
        }
        if ($category == 'pd') {
            $category = 'P';
        }

        $query = $this->db->createQueryBuilder();

        $query
            ->select('*')
            ->from('listing')
            ->orderBy("$sort", "$order")
            ->where('(status = (:status) OR status = (:pend)) AND category LIKE (:category)')
            ->setParameter('status', $status)
            ->setParameter('pend', $pending)
            ->setParameter('category', '%' . $category . '%');

        return $query->execute()->fetchAll();
    }

    public function getSimilarListings($price, $category)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('*')
            ->from('listing')
            ->orderBy('id', 'DESC')
            ->where("
            status = (:status)
            OR status = (:pend)
            AND category LIKE (:category)
            AND listing_price < ((:price) + 50000)
            AND listing_price > ((:price) - 50000)"
            )
            ->setMaxResults(3)
            ->setParameter('status', 'available')
            ->setParameter('pend', 'pending')
            ->setParameter('price', $price)
            ->setParameter('category', '%' . $category . '%');

        return $query->execute()->fetchAll();
    }

    // returns trucks related to the entities it is provided
    // entities must be structured as an array of primary key ids
    public function getRelatedTrucksFiltered($entityKeys)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('t.id,t.fxg_num,t.year,t.make,t.odometer,e.name, t.perc_completed as perc')
            ->from('truck', 't')
            ->orderBy('fxg_num', 'ASC')
            ->where('entity IN (:ids)')
            ->join('t', 'entity', 'e', 't.entity = e.id')
            ->setParameter('ids', $entityKeys, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);

        $result = $query->execute()->fetchAll();

        foreach ($result as $key => $value) {
            $perc = $value['perc'] * 100;
            $result[$key]['perc'] = $perc;
            if ($perc < 70) {
                $result[$key]['color'] = 'danger';
            } elseif ($perc < 90) {
                $result[$key]['color'] = 'warning';
            } else {
                $result[$key]['color'] = 'success';
            }
        }

        return $result;
    }

    public function getAllMakes()
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, name')
            ->from('truck_make');

        return $query->execute()->fetchAll();
    }

    public function getPercThroughPM($truck_key, $curr_odometer)
    {

        // get the most recent pm
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id, mileage, date')
            ->from('maint_event')
            ->orderBy('date', 'DESC')
            ->where('maint_service = (:service) AND truck = (:truck)')
            ->setParameter('service', '34')
            ->setParameter('truck', $truck_key);

        $lastPM = floatval($query->execute()->fetch()['mileage']);

        // get the pm lifespan
        $query = $this->db->createQueryBuilder();

        $query
            ->select('miles')
            ->from('maint_interval')
            ->where('maint_service = (:service)')
            ->setParameter('service', '34');

        $frequency = floatval($query->execute()->fetch()['miles']);

        // $nextPM = $lastPM + $frequency;
        $nextPM = $lastPM + $frequency;

        $milesToPM = $nextPM - $curr_odometer;

        if ($nextPM < $curr_odometer) {
            return 100;
        } else {
            return number_format($milesToPM / $frequency * 100, 0);
        }

    }

    public function getCurrentMileage($truck_key)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('odometer')
            ->from('truck')
            ->where('id = (:key)')
            ->setParameter('key', $truck_key);

        return floatval($query->execute()->fetch()['odometer']);
    }

}

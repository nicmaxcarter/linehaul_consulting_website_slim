<?php

namespace App\Models;

use Doctrine\DBAL\Connection as DB;

class Subscriber
{

    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getSubscriberId($email)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id')
            ->from('subscriber')
            ->where('email = (:email)')
            ->setParameter('email', $email);

        return $query->execute()->fetch()['id'];
    }

    public function addSubscriber($email)
    {
        // add subscriber to existing list
        try {
            $id = $this->storeSubscriber($email);
        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {
            return false;
        }

        return true;
    }

    public function storeSubscriber($email)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->insert('subscriber')
            ->values(
                array(
                    'email' => '?',
                )
            )
            ->setParameter(0, $email);

        $query->execute();

        return $this->db->lastInsertId();
    }

}

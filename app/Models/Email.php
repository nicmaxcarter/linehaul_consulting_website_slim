<?php 

namespace App\Models;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email {
	
	protected $mail;
	
	function __construct() {
		$this->mail = new PHPMailer();
		$this->mail->IsSMTP(); // enable SMTP
		$this->mail->SMTPDebug = 0; // debugging: 1 = errors and messages, 2 = messages only
		$this->mail->SMTPAuth = true; // authentication enabled
		$this->mail->SMTPSecure = 'tls'; // secure transfer enabled REQUIRED for Gmail
		$this->mail->Host = "west.exch091.serverdata.net";
		$this->mail->Port = 587; // or 587
		$this->mail->IsHTML(true);
		$this->mail->Username = "max@linehaulconsulting.com";
		$this->mail->Password = "FedEx19$";
		$this->mail->SetFrom("max@linehaulconsulting.com");
	}
	
	function setSubject($str) {
		$this->mail->Subject = $str; 
	}
	
	function setBody($str){
		$this->mail->Body = $str;
	}
	
	function addRecipient($str){
		$this->mail->AddAddress($str);
    }
    
    function addBCC($str){
        $this->mail->addBCC($str);
    }
	
	function setFromName($str){
		$this->FromName = $str;
	}
	
	function setDebug($int){
		$this->mail->SMTPDebug = $int; // debugging: 1 = errors and messages, 2 = messages only
	}
	
	function send(){
		try{
			return $this->mail->send();
			// echo "\n\nMessage sent\n\n";
		}catch (Exception $e){
			/* PHPMailer exception. */
			echo $e->errorMessage();
		} catch (\Exception $e){
			/* PHP exception (note the backslash to select the global namespace Exception class). */
			echo $e->getMessage();
		} 
	}
	
	function test(){
		// $this->email->test();
        // return true;
        $this->setSubject("Thank you for signing up!");
        $this->setBody("Yo thanks bro!");
        $this->addRecipient("nicmaxcarter@gmail.com");
        $this->setFromName("Max");
        $this->send();
        return true;
	}
	
}


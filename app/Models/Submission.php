<?php

namespace App\Models;

use Doctrine\DBAL\Connection as DB;

class Submission
{

    protected $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getSubscriberId($email)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->select('id')
            ->from('subscriber')
            ->where('email = (:email)')
            ->setParameter('email', $email);

        return $query->execute()->fetch()['id'];
    }

    public function storeSubmission($fname, $lname, $email, $phone, $message)
    {
        $query = $this->db->createQueryBuilder();

        $query
            ->insert('submissions')
            ->values(
                array(
                    'first_name' => '(:fname)',
                    'last_name' => '(:lname)',
                    'email' => '(:email)',
                    'phone' => '(:phone)',
                    'message' => '(:message)',
                    'status' => '(:status)',
                )
            )
            ->setParameter('fname', $fname)
            ->setParameter('lname', $lname)
            ->setParameter('email', $email)
            ->setParameter('phone', $phone)
            ->setParameter('status', 'unread')
            ->setParameter('message', $message);

        $query->execute();

        // return $this->db->lastInsertId();
        return true;
    }

    public function sendThankYouEmail($mail, $emailAddress)
    {
        // $mail->setSubject("Thank you for signing up!");
        // $mail->setBody("Yo thanks bro!");
        // $mail->addRecipient($emailAddress);
        // $mail->setFromName("Max");
        // $mail->send();
        return true;
    }

    public function sendSubmissionEmail($mail,$fname, $lname, $email, $phone, $message)
    {
        $mail->setSubject("Consulting: New Contact Submission!");
        $mail->setBody("Go check out your submissions!");
        $mail->addRecipient('max@linehaulconsulting.com');
        $mail->setFromName("Max");
        $mail->send();
        return true;
    }
}

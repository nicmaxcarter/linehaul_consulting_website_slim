<?php

namespace App\Controllers;

use App\Models\Fleet as Fleet;
use App\Models\Listing as Listing;
use App\Models\User as User;
use App\Models\Email as Email;
use Monolog\Logger as Log;
use App\Models\Subscriber as Subscriber;
use App\Models\Submission as Submission;
use App\Controllers\EmailController as Emailer;
use Psr\Http\Message\ResponseInterface as Response;

class APIController
{

    // protected $base_url = "http://newlinehaul.conichost.com";
    // protected $token = "Bearer iJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNTY0NDU3M";

    public function addSubscriber(Subscriber $subscriber, Log $log, Emailer $emailer, Response $response, User $user, Fleet $fleet)
    {
        $emailAddress = json_decode(file_get_contents("php://input"))->val;
        $log->info("New subscriber is being added: $emailAddress");
        $res = $subscriber->addSubscriber($emailAddress);
        // $res = $emailer->sendEmail($subscriberId);
        $response->getBody()->write(json_encode($res));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getRoutesDefault(Response $response, Listing $list, $status) {
        $routes = $list->getListings($status, 'none', '');

        $response->getBody()->write(json_encode($routes));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getRoutesSorted(Response $response, Listing $list, $status, $order, $category) {
        $routes = $list->getListings($status, $order, $category);

        $response->getBody()->write(json_encode($routes));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function submitContactForm(Response $response, Submission $submission, Email $mail) {

        // return json_encode(array(true));

        $data = json_decode(file_get_contents("php://input"));

        $fname = $data->first_name;
        $lname = $data->last_name;
        $email = $data->email;
        $phone = $data->phone;
        $message = $data->message;

        $store = $submission->storeSubmission(
            $fname,$lname,$email,$phone,$message
        );

        $thankResult = $submission->sendThankYouEmail($mail, $email);

        $mailResult = $submission->sendSubmissionEmail(
            $mail,$fname,$lname,$email,$phone,$message
        );

        $result = false;

        if($store && $mailResult && $thankResult) {
            $result = true;
        }

        $response->getBody()->write(json_encode($result));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getRoute(Response $response, Listing $list, $id) {
        $route = $list->getIndividualListing($id);
        $response->getBody()->write(json_encode($route));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getSimilarRoutesByPrice(Response $response, Listing $list, $price, $category) {
        $routes = $list->getSimilarListings($price, $category);
        $response->getBody()->write(json_encode($routes));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }




    public function callApi($method, $url, $data)
    {
        $curl = curl_init();

        switch ($method) {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }

            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if ($data) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }

            break;
        default:
            if ($data) {
                $url = sprintf("%s?%s", $url, http_build_query($data));
            }

        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            "Authorization: $this->token",
            // "Authorization: Bearer staticToken",
            "Content-Type: application/json",
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        // print_r($result);
        if (!$result) {die("Connection Failure");}
        curl_close($curl);
        return $result;
    }


}

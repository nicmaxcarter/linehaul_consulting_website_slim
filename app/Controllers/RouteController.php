<?php

namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig as View;

class RouteController
{
    public function getHome(Response $response, View $view)
    {
        return $view->render($response, 'home_2.html');
    }

    public function renderRoutes(Response $response, View $view)
    {
        return $view->render($response, 'routes.html', [
            'status' => 'available'
        ]);
    }

    public function renderSold(Response $response, View $view)
    {
        return $view->render($response, 'sold_routes.html', [
            'status' => 'sold'
        ]);
    }

    public function renderSell(Response $response, View $view)
    {
        return $view->render($response, 'sell.html', [
            'status' => 'sold'
        ]);
    }

    public function renderRouteIndividual(Response $response, View $view, $id)
    {
        return $view->render($response, 'route_individual.html', [
            'id' => $id
        ]);
    }

    public function renderContact(Response $response, View $view)
    {
        return $view->render($response, 'contact.html');
    }

    public function renderAdmin(Response $response)
    {
        return $response
            ->withHeader('Location', 'https://linehaul.dev')
            ->withStatus(302);
    }

    public function renderMaintenance(Response $response)
    {
        return $response
            ->withHeader('Location', 'https://linehaul.dev')
            ->withStatus(302);
    }
}

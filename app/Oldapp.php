<?php

namespace App;

use DI\ContainerBuilder as ContainerBuilder;
use Psr\Container\ContainerInterface as ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
// use Monolog\Logger as Logger;

class OldApp extends \DI\Bridge\Slim\App
{
    protected function configureContainer(ContainerBuilder $builder)
    {
        $definitions = [
            'settings.displayErrorDetails' => true,
            // Database connection with Doctrine DBAL
            \Doctrine\DBAL\Connection::class => function (ContainerInterface $container) {
                $config = new \Doctrine\DBAL\Configuration();
                $connectionParams = array(
                    'dbname' => 'conich6_linehaul_consulting',
                    'user' => 'conich6_linehaul_admin',
                    'password' => 'FedEx2310@#!',
                    'host' => 'conichost.com',
                    'driver' => 'pdo_mysql',
                );
                return \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
            },
            \Delight\Auth\Auth::class => function () {
                $db = new \PDO(
                    'mysql:dbname=conich6_wrench_rite;host=conichost.com;charset=utf8mb4',
                    'conich6_linehaul_admin',
                    'FedEx2310@#!'
                );

                return new \Delight\Auth\Auth($db);
            },
            \Slim\Views\Twig::class => function (ContainerInterface $container) {
                $view = new \Slim\Views\Twig(__DIR__ . '/../templates', [
                    'cache' => __DIR__ . '/../tmp/twig-cache',
                    // 'cache' => false,
                ]);

                $view->addExtension(new \Slim\Views\TwigExtension(
                    $container->get('router'),
                    $container->get('request')->getUri()
                ));
                $view->getEnvironment()->addGlobal('assets', '/assets');
                if (isset($_SESSION['displayName'])) {

                    $view->getEnvironment()->addGlobal('displayName', $_SESSION['displayName']);
                }

                return $view;
            },
            \Monolog\Logger::class => function (ContainerInterface $container) {
                $logger = new \Monolog\Logger('app');
                $logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__ . '/../tmp/logs/app.log', \Monolog\Logger::DEBUG));
                return $logger;
            },
            \Slim\Flash\Messages::class => function (ContainerInterface $container) {
                return new \Slim\Flash\Messages();
            },
            'notFoundHandler' => function (ContainerInterface $container) {
                return function ($request, $response) use ($container) {
                    return $container->get(\Slim\Views\Twig::class)->render($response, '404.htm');
                };
            }
        ];

        $builder->addDefinitions($definitions);



    }

}

<?php

namespace App\Middleware;
use Psr\Container\ContainerInterface as ContainerInterface;

class Middleware
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
}

<?php

namespace App\Middleware;

use Slim\Views\Twig as View;
use Slim\Flash\Messages as Flash;

class FlashMiddleware
{

    protected $container;

    public function __construct($container){
        $this->container = $container;
    }

    public function __invoke($request, $response, $next)
    {
        var_dump($this->container->flash);
        die();
        $view->offsetSet("flash", $flash->getMessages());
        $response = $next($request, $response);

        return $response;
    }
}

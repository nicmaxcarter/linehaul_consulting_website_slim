<?php

namespace App\Middleware;

use Slim\Route as Route;
use Psr\Container\ContainerInterface as ContainerInterface;

class ExampleMiddleware
{
    protected $container;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
    }
    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next, Route $route)
    {

        $route = $request->getAttribute('route');
        $routeName = $route->getName();
        $groups = $route->getGroups();
        $methods = $route->getMethods();
        $arguments = $route->getArguments();

        // if(isset($_SESSION['auth_logged_in'])){
        return $next($request, $response);
        // } else{
        //     return $response->withRedirect('/admin/auth/signin', 301);
        // }

    }
}

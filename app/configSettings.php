<?php

// $localDevSettings = [
//     'cache'     => false,
//     'assets'    => '/assets',
//     'db'        => [
//         'dbname'    => 'lhc_argon',
//         'user'      => 'lhc_admin_argon',
//         'password'  => '%x!rEa-{>8Uk>;{u',
//         'host'      => 'data.linehaul.dev',
//         'driver'    => 'pdo_mysql',
//     ],
// ];
$localDevSettings = [
    'twig'     => [
        'cache' => false
    ],
    'assets'    => '/assets',
    'db'        => [
        'dbname'    => 'consulting',
        'user'      => 'root',
        'password'  => 'Flatland123!',
        'host'      => 'localhost',
        'driver'    => 'pdo_mysql',
    ],
];

$liveDeploySettings = [
    'twig'     => [
        'cache'     => __DIR__ . '/../tmp/twig-cache'
    ],
    // key cdn
    'assets'    => 'https://linehauldev-13a6a.kxcdn.com/assets',
    'db'        => [
        'dbname'    => 'lhc_argon',
        'user'      => 'lhc_admin_argon',
        'password'  => '%x!rEa-{>8Uk>;{u',
        'host'      => '155.138.238.92',
        'driver'    => 'pdo_mysql',
    ],
];


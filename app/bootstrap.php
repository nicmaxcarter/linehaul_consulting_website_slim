<?php

declare(strict_types=1);
use DI\ContainerBuilder;
use Slim\Views\TwigMiddleware;

// Set the absolute path to the root directory.
$rootPath = realpath(__DIR__ . '/..');

// Include the composer autoloader.
include_once($rootPath . '/vendor/autoload.php');


// Start PHP session
session_start();

// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

$settings = require $rootPath . '/app/settings.php';
$settings($containerBuilder);

$build = require $rootPath . '/app/App.php';
$build($containerBuilder);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

//$app = new \App\App;
$app = \DI\Bridge\Slim\Bridge::create($container);

$parser = $app->getRouteCollector()->getRouteParser();
$app->getContainer()->set(\Slim\Routing\RouteParser::class, $parser);

$errorMiddleware = $app->addErrorMiddleware(true, true, true);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create(
    $app,
    $container->get(\Slim\Views\Twig::class)
));

$routes = require 'routes.php';
$routes($app);

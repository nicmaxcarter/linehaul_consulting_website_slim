<?php

use DI\ContainerBuilder as ContainerBuilder;
use Psr\Container\ContainerInterface as ContainerInterface;

return function (ContainerBuilder $builder)
{
    $definitions = [
        // Database connection with Doctrine DBAL
        \Doctrine\DBAL\Connection::class => function (ContainerInterface $container) {
            $config = new \Doctrine\DBAL\Configuration();
            $connectionParams = $container->get('settings')['db'];
            return \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
        },
        \Delight\Auth\Auth::class => function (ContainerInterface $container) {
            $config = $container->get('settings')['db'];

            $db = new \PDO(
                'mysql:dbname='. $config['dbname'] .';'.
                'host='. $config['host'] .';' .
                'charset=utf8mb4',
                $config['user'],
                $config['password']
            );

            return new \Delight\Auth\Auth($db);
        },
        \Slim\Views\Twig::class => function (ContainerInterface $container) {
            $view = \Slim\Views\Twig::create(
                __DIR__ . '/../templates',
                    $container->get('settings')['twig']
            );

            // path to all assets. This may be useful later
            $view->getEnvironment()->addGlobal(
                'assets',
                $container->get('settings')['assets']
            );

            if (isset($_SESSION['displayName'])) {
                $view->getEnvironment()->addGlobal('displayName', $_SESSION['displayName']);
            }

            if (isset($_SESSION['owner'])) {
                if ($_SESSION['owner']) {
                    $view->getEnvironment()->addGlobal('owner', $_SESSION['owner']);
                }
            }

            return $view;
        },
        \Monolog\Logger::class => function (ContainerInterface $container) {
            $logger = new \Monolog\Logger('app');
            $logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__ . '/../tmp/logs/app.log', \Monolog\Logger::DEBUG));
            return $logger;
        },
        \Slim\Flash\Messages::class => function (ContainerInterface $container) {
            return new \Slim\Flash\Messages();
        },
        'notFoundHandler' => function (ContainerInterface $container) {
            return function ($request, $response) use ($container) {
                return $container->get(\Slim\Views\Twig::class)->render($response, 'maintenance/expenseReportForm.html');
            };
        },
    ];

    // require_once __DIR__ . '/config.php';
    $builder->addDefinitions($definitions);
    $builder->addDefinitions(require_once __DIR__ . '/slimConfig.php');
};


<?php

declare(strict_types=1);

use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {

    // this should be the only variable to change when switching from
    // development to deployment
    $development = true;

    require_once __DIR__ . '/configSettings.php';

    $configParams = [];

    if ($development) {
        $configParams = $localDevSettings;
    } else {
        $configParams = $liveDeploySettings;
    }

    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            // Base path
            'base_path' => '',
            // cache
            'twig' => $configParams['twig'],
            // assets
            'assets' => $configParams['assets'],
            // db
            'db' => $configParams['db']
        ]
    ]);

    //if ($environment == 'prod') { // Should be set to true in production
        //$containerBuilder->enableCompilation($settings['compile']);
    //}
};

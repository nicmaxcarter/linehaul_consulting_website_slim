function addRoutes(status) {
  // return new Promise(function (resolve, reject) {
  getRoutes(status).then(resolve => populateRoutes(resolve));
  // }
}

function getRoutes(category) {
  return new Promise(function (resolve, reject) {
    var url = `/routes/${category}`;
    // console.log(url);
    fetch(url, {
      method: 'POST'
    })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        resolve(data);
      })
  })
}
function getSortedRoutes(category, sort, filter) {
  return new Promise(function (resolve, reject) {
    var url = `/routes/${category}/${sort}/${filter}`;
    console.log(url);
    // return;
    fetch(url, {
      method: 'POST'
    })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        resolve(data);
      })
  })
}

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function populateRoutes(routes) {
  return new Promise(function (resolve, reject) {
    // console.log(routes);

    var mainDiv = document.getElementById('routeDiv');
    mainDiv.innerHTML = '';
    
    if(routes.length == 0){
        mainDiv.innerHTML = `
        <div class="container text-center">
                    <div class="row">
                        <div class="col pt-5">
                            <p class="lead" style="font-size: 1.3rem;">We currently have no routes available to sell.</p>
                            <p class="lead">
                        Take a look at routes that we have recently sold. It might give some insight.
                    </p>
                    <p class="lead">
                        <a href="/soldFedExRoutes">View More</a>
                    </p>
                        </div>
                    </div>
                    
                </div>
                `;
    } else {

    routes.forEach(element => {
      // console.log(element);
      var price = numberWithCommas(element.listing_price);

      var pending = '';
      if(element.status == 'pending'){
        pending = getPendingLabel();
      }

      var routeLabel = 'Routes';
      var truckLabel = 'Trucks';
      if (element.routes_for_sale == 1) { routeLabel = 'Route'; }
      if (element.tractors_for_sale == 1) { truckLabel = 'Truck'; }
      

      var content = `
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 my-4">
          <a class="route-link" href="/route/${element.id}">
            <div class="fdb-box fdb-touch p-0 shadow-hover">
              
              <div class="p-3">
                <h2 class="pt-2 mb-0 pb-0">${element.state}</h2>
                <h3 class="text-muted mt-0 pt-0 pb-2">${element.category}</h3>
                <p class="m-0">Listing Price: $${price}</p>
                <p class="m-0">${element.routes_for_sale} ${routeLabel} / ${element.tractors_for_sale} ${truckLabel}</p>
                <div class="row">
                  <div class="col">
                    <a class="btn btn-xs btn-primary mt-4 mb-1 mb-lg-2" href="/route/${element.id}"
                    style="color: #ffffff">More Info
                    </a>
                  </div>
                  ${pending}
                </div>
              </div>
            </div>
          </a>
        </div>
      `;

      mainDiv.innerHTML += content;

    });
    }

    resolve("done with populate");
  });
}

function getPendingLabel() {
  return `
  <div class="col" style="
  display: flex;
  align-items: center;">
  <button class="bttn-unite bttn-xs bttn-success mt-4 mb-1 mb-lg-2">
    Sale Pending
  </button>
  </div>
  `;
}

function displayMaps() {
  return new Promise(function (resove, reject) {

    var maps = document.getElementsByClassName('map');

    for (let element of maps) {

      var lat = parseFloat(element.getAttribute('lat'));
      var lng = parseFloat(element.getAttribute('lng'));

      var mymap = L.map(element, { zoomControl: false, closePopupOnClick: false, dragging: false, scrollWheelZoom: false }).setView([lat, lng], 12);
      // L.zoomControl(false).addTo(mymap);

      L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiY29kb2JpdGRldiIsImEiOiJjandxM3MweWswZGM1NDRxaWhsbXliNmxzIn0.14ZaScXQBXrVNnkOylm0zA', {
        attribution: '',
        maxZoom: 18,
        id: 'mapbox.light',
        accessToken: 'pk.eyJ1IjoiY29kb2JpdGRldiIsImEiOiJjandxM3MweWswZGM1NDRxaWhsbXliNmxzIn0.14ZaScXQBXrVNnkOylm0zA'
      }).addTo(mymap);

      L.marker([lat, lng]).addTo(mymap);
    }
  })
}


function addResetButton(status) {
  if (document.getElementById('reset-col') == null) {
    var sortRow = document.getElementById('sort-row');
    var resetColumn = document.createElement('div');
    resetColumn.setAttribute('id', 'reset-col');
    resetColumn.setAttribute('class', 'col mt-4');
    resetColumn.innerHTML = `
                        <button type="button" id="reset-button" class="btn btn-sm btn-primary"
                        onclick="resetRadios('${status}')">Reset</button>
                        `;
    sortRow.appendChild(resetColumn);
  }
}

function checkRadios(status) {

  addResetButton(status);


  var sortButtons = document.getElementsByName("sort-option");
  var filterButtons = document.getElementsByName("filter-option");
  var selectedSort, selectedFilter;

  for (var i = 0; i < sortButtons.length; i++) {
    if (sortButtons[i].checked)
      selectedSort = sortButtons[i].value;
  }
  for (var i = 0; i < filterButtons.length; i++) {
    if (filterButtons[i].checked)
      selectedFilter = filterButtons[i].value;
  }

  if (selectedSort == undefined) {
    selectedSort = 'none';
  }
  if (selectedFilter == undefined) {
    selectedFilter = 'none';
  }

  // getSortedRoutes('available',selectedSort,selectedFilter)
  // .then(
  //     resolve => populateRoutes(resolve)
  // );
  getSortedRoutes(status, selectedSort, selectedFilter)
    .then(
      resolve => populateRoutes(resolve)
    );

}

function resetRadios(status) {
  // remove the reset button
  document.getElementById('reset-col').remove();
  // get the sort option radio buttons
  var sortOptions = document.getElementsByName('sort-option');
  // get the filter option radio buttons
  var filterOptions = document.getElementsByName('filter-option');
  // join both together in one array
  var options = Array.from(sortOptions).concat(Array.from(filterOptions));
  // iterate through and set all radio buttons to false
  for(var i=0; i < options.length; i++) {
    options[i].checked = false;
  }

  // get the bootstrap labels to reset them
  var labels = document.getElementsByClassName('active');
  var arr = [].slice.call(labels);
  // console.log(arr);

  arr.forEach(function (element) {
    // console.log(element);
    element.classList.remove('active');
  })
    

  getRoutes(status).then( // get the available routes
    resolve => populateRoutes(resolve));
}

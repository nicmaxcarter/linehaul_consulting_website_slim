function addSubscriberEvent(form)
{
  form.addEventListener('submit', function (e) {
    e.preventDefault();
    showLoad();

    var email = e.target[0].value;

    if (e.target.length == 0) {
      alert("something went wrong");
    } else {
      submitSubscriberEmail(email).then(resolve => {
        if (resolve) {
          showThanks();
          sendWelcomeEmail();
        } else {
          showError();
        }
      });
    }
  })
}

function showLoad() {
    var load = document.getElementById('loader');
    var head = document.getElementById('subscribe-header');
    var subhead = document.getElementById('subscribe-subhead');
    var content = document.getElementById('subscribe-content');

    head.innerHTML = "&nbsp;";
    subhead.innerHTML = "&nbsp;";
    content.classList.add('hide');
    load.classList.remove("hide");

  }

  function showThanks() {
    var load = document.getElementById('loader');
    var head = document.getElementById('subscribe-header');
    var subhead = document.getElementById('subscribe-subhead');

    load.classList.add("hide");

    setTimeout(function () {
      head.innerHTML = "Thank You";
      subhead.innerHTML = "Now you'll get notified of available routes!";
      head.classList.add("fadeIn");
      subhead.classList.add("fadeIn");
    }, 200);
  }

  function showError() {
    var load = document.getElementById('loader');
    var head = document.getElementById('subscribe-header');
    var subhead = document.getElementById('subscribe-subhead');

    load.classList.add("hide");

    setTimeout(function () {
      head.innerHTML = "Whoops!";
      subhead.innerHTML = "It seems that there has been an error";
      head.classList.add("fadeIn");
      subhead.classList.add("fadeIn");
    }, 200);
  }


  function submitSubscriberEmail(email) {
    return new Promise(function (resolve, reject) {
      var url = "/addsubscriber";
      fetch(url, {
        method: 'POST',
        body: JSON.stringify({ val: email })
      })
        .then(response => response.json())
        .then(json => {
          // console.log(json);

          try {
            if (json) {
              resolve(true);
            } else {
              resolve(false);
            }
          }
          catch (error) {
            resolve(false);
          }

        })
    })
  }
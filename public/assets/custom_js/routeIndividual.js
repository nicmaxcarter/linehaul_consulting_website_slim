function getRoute(id) {
  return new Promise(function (resolve, reject) {
    var url = "/routeInfo/" + id;
    // console.log(url);
    fetch(url, {
      method: 'POST'
    })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        resolve(data);
      })
  })
}

function populatePage(data) {
  // displayMap(data.location.lat, data.location.lng);
  var state = document.getElementById('route-state');
  state.innerHTML = data.state;
  var city = document.getElementById('route-category');
  city.innerHTML = data.category;
  var city = document.getElementById('listing-number');
  city.innerHTML = data.listing_number;

  // populate number of routes
  document.getElementById('routes-for-sale').innerHTML = data.routes_for_sale;
  // if routes is less than 2, change to 'route'
  if (data.routes_for_sale == 1) {
    document.getElementById('route-label').innerHTML = 'Route';
  }
  // populate number of trucks
  document.getElementById('trucks-for-sale').innerHTML = data.tractors_for_sale;
  // if trucks is less than 2, change to 'truck'
  if (data.tractors_for_sale == 1) {
    document.getElementById('truck-label').innerHTML = 'Truck';
  }

  // populate revenue
  if (data.revenue != null) {
    document.getElementById('revenue').innerHTML = numberWithCommas(data.revenue);
    document.getElementById('perc-of-revenue').innerHTML = Math.round(data.listing_price / data.revenue * 100);
  }
  // populate listing price
  document.getElementById('listing-price').innerHTML = numberWithCommas(data.listing_price);
  // calculate listing price percentage

  // set default fleet included
  if (data.trucks_included == 1) {
    document.getElementById('fleet-included').innerHTML = 'Fleet included in sale';
    // set default fleet value
    if (data.fleet_value) {
		var commaSeparatedValue = numberWithCommas(data.fleet_value);
      document.getElementById('fleet-value').innerHTML = `Fleet valued at $${commaSeparatedValue}`;
    } else {
      document.getElementById('fleet-value').innerHTML = `Fleet valuation available upon request`;
    }
  } else {
    document.getElementById('fleet-value').setAttribute('style', 'display:none;');
    document.getElementById('fleet-included').innerHTML = 'Fleet not included in sale';
  }



  // populate other information
  if (data.more_information != null) {
    document.getElementById('information').innerHTML = data.more_information;
  } else {
    document.getElementById('information').setAttribute('style', 'display:none;');
  }

  // set pending if true
  if (data.status == 'pending') {
    document.getElementById('sale-pending').setAttribute('style', 'display: inline-block');
  }

  if (data.cash_flow != null && data.cash_flow > 100) {
  document.getElementById('cash-flow').innerHTML = numberWithCommas(data.cash_flow);
  } else {
    document.getElementById('cash-flow-col').setAttribute('style','display:none');
  }


  // console.log(data);
}

function getSimilarRoutes(price, category) {
  return new Promise(function (resolve, reject) {
    var url = `/similarRoutes/${price}/${category}`;
    // console.log(url);
    fetch(url, {
      method: 'POST'
    })
      .then(response => response.json())
      .then(data => {
        // console.log(data);
        resolve(data);
      })
  })
}


function populateSimilarRoutes(routes) {
  return new Promise(function (resolve, reject) {
    // console.log(routes);

    var mainDiv = document.getElementById('more-routes-row');
    mainDiv.innerHTML = '';

    routes.forEach(element => {
      // console.log(element);
      var price = numberWithCommas(element.listing_price);

      var pending = '';
      if(element.status == 'pending'){
        pending = getPendingLabel();
      }

      var routeLabel = 'Routes';
      var truckLabel = 'Trucks';
      if (element.routes_for_sale == 1) { routeLabel = 'Route'; }
      if (element.tractors_for_sale == 1) { truckLabel = 'Truck'; }
      

      var content = `
        <div class="col-sm-12 col-md-4 col-lg-3 my-4">
          <div class="fdb-box fdb-touch p-0 shadow-hover"><a class="route-link" href="/route/2">

              <div class="p-3">
                <a class="route-link" href="/route/${element.id}">
                  <h2 class="pt-2 mb-0 pb-0">${element.state}</h2>
                  <h3 class="text-muted mt-0 pt-0 pb-2">${element.category}</h3>
                  <p class="m-0">Listing Price: $${price}</p>
                  <p class="m-0">${element.routes_for_sale} ${routeLabel} / ${element.tractors_for_sale} ${truckLabel}</p>
                </a>
                <div class="row">
                  <div class="col">
                    <a class="btn btn-xs btn-primary mt-4 mb-1 mb-lg-2" href="/route/${element.id}"
                    style="color: #ffffff">More Info
                    </a>
                  </div>
                  ${pending}
                </div>
              </div>
          </div>
        </div>
      `;

      mainDiv.innerHTML += content;

    });

    resolve("done with populate");
  });
}



function displayMap(lat, lng) {

  var map = document.getElementById('map');

  var mymap = L.map(map, { zoomControl: false, closePopupOnClick: false, dragging: false, scrollWheelZoom: false }).setView([lat, lng], 12);
  // L.zoomControl(false).addTo(mymap);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiY29kb2JpdGRldiIsImEiOiJjandxM3MweWswZGM1NDRxaWhsbXliNmxzIn0.14ZaScXQBXrVNnkOylm0zA', {
    attribution: '',
    maxZoom: 18,
    id: 'mapbox.light',
    accessToken: 'pk.eyJ1IjoiY29kb2JpdGRldiIsImEiOiJjandxM3MweWswZGM1NDRxaWhsbXliNmxzIn0.14ZaScXQBXrVNnkOylm0zA'
  }).addTo(mymap);

  L.marker([lat, lng]).addTo(mymap);
}

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
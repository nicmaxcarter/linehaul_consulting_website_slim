function addContactEvent(form) {
  form.addEventListener('submit', function (e) {
    e.preventDefault();
    showLoad();

    var data = collectFormData(e.target);

    if (e.target.length == 0) {
      alert("something went wrong");
    } else {
      submitForm(data).then(resolve => {
        if (resolve) {
          showThanks();
        } else {
          showError();
        }
      });
    }
  })
}

function addhyphen() {
  var form = document.getElementById('contact-form');
  str = form[3].value;
  if (str.length == 7) {
    str = str.substring(0, 3) + "-" + str.substring(3, str.length);
  }
  if (str.length == 10) {
    str = str.substring(0, 3) + "-" + str.substring(3, 6) + "-" + str.substring(6, str.length);
  }
  form[3].value = str;
}

function collectFormData(form) {
  var data = {
    "first_name": form[0].value,
    "last_name": form[1].value,
    "email": form[2].value,
    "phone": form[3].value,
    "message": form[4].value
  };

  return data;
}

function showLoad() {
  var load = document.getElementById('loader');
  var form = document.getElementById('contact-form');

  form.classList.add('hide');
  load.classList.remove("hide");

}

function showThanks() {
  var load = document.getElementById('loader');
  var thanks = document.getElementById('thanks');

  load.classList.add("hide");

  setTimeout(function () {
    thanks.classList.remove("hide");
    thanks.classList.add("fadeIn");
  }, 200);
}

function showError() {
  var load = document.getElementById('loader');
  var error = document.getElementById('error');

  load.classList.add("hide");

  setTimeout(function () {
    error.classList.remove("hide");
    error.classList.add("fadeIn");
  }, 200);
}

function submitForm(data) {
  return new Promise(function (resolve, reject) {
    var url = "/submitContact";
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data)
    })
      .then(response => response.text())
      .then(data => {
        console.log(data);
        try {
          if (data) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
        catch (error) {
          resolve(false);
        }
      })
  })
}